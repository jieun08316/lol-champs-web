import Vue from 'vue'

import LoadingIcon from '~/components/common/loading-icon'
Vue.component('LoadingIcon', LoadingIcon)

import LolChamps from '~/components/common/lol-champs.vue'
Vue.component('LolChamps', LolChamps)
